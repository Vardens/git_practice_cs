﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GitPractice.Startup))]
namespace GitPractice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
